from setuptools import setup, find_packages

setup(name='whisp-django-commons',
      version='0.3.1',
      description='Whisp\'s professional first class common utilities for django apps',
      url='https://github.com/whisprnd/whisp-django-commons',
      author='Whisp R&D',
      author_email='or@whisp.software',
      license='private',
      packages=find_packages(),
      install_requires=[
            'django >= 2'
      ],
      extras_require = {

      },
      zip_safe=False)