import re

from django.forms import HiddenInput, Widget

DJ_MONEY_FEATURES = False

try:
    from djmoney.forms import MoneyWidget

    DJ_MONEY_FEATURES = True
except ImportError:
    pass


def camelize(string: str):
    def underscoreToCamel(match):
        return match.group()[0] + match.group()[2].upper()

    return re.sub(r"[a-z]_[a-z]", underscoreToCamel, string)

if DJ_MONEY_FEATURES:
    class AmountOnlyMoneyWidget(MoneyWidget):
        def __init__(self, *args, **kwargs):
            kwargs.update(currency_widget=HiddenInput())
            super().__init__(*args, **kwargs)


class DynamicWidgetDecorator(Widget):
    get_new_data_url_attribute = 'data-get-new-data-url'

    def decorate(self, widget, field_name, source_field_name, get_new_data_url):
        self.field_name = field_name
        widget.attrs.update(
            {
                'data-source-field': source_field_name,
                'data-get-new-data-url': get_new_data_url,
                'data-trigger-function': self.get_trigger_function_name(),
            }
        )
        return widget

    def get_trigger_function_name(self):
        in_camel_case = camelize(self.field_name)
        return f'update{in_camel_case[0].upper()}{in_camel_case[1:]}'


class DynamicChoicesWidgetDecorator(DynamicWidgetDecorator):
    def get_trigger_function_name(self):
        return 'updateChoices'
