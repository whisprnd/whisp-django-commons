function updateChoices($dynamicField, getNewChoicesUrl, sourceValueId) {
    const selectedValue = $dynamicField.find('option[selected]').val();

    $dynamicField.empty();
    $.get(getNewChoicesUrl, data = {
        'pk': sourceValueId
    }).then((data) => {
        const $emptyOption = $dynamicField.append($("<option></option>").attr("value", "").text("---------"));
        for (const key of Object.keys(data)) {
            $dynamicField.append($("<option></option>").attr("value", key).text(data[key]));
        }
        if (data[selectedValue]) {
            $dynamicField.find(`option[value=${selectedValue}]`).attr("selected", true);
        } else {
            $emptyOption.attr("selected", true);
        }
    })
}

function setupDynamicModelFields() {
    $('[data-source-field]').each((index, element) => {
        const sourceField = element.dataset.sourceField;
        const updateFunctionName = element.dataset.triggerFunction;

        //TODO: Improve
        const updateFunction = window[updateFunctionName];
        if (!updateFunction) {
            console.error(`Couldn't find update function named ${updateFunctionName}`);
            return;
        }

        const $dynamicField = $(element);
        const $sourceField = $(`#id_${sourceField}`);
        const sourceValue = $sourceField.val();
        const getNewDataUrl = $dynamicField[0].dataset.getNewDataUrl;
        $sourceField.change(() => {
            const copiedSourceValue = $sourceField.val();
            return copiedSourceValue && updateFunction($dynamicField, getNewDataUrl, copiedSourceValue);
        });

        if (sourceValue) {
            updateFunction($dynamicField, getNewDataUrl, sourceValue);
        }
    });
}

if (!$) {
    $ = django.jQuery;
}

$(setupDynamicModelFields);
