import logging
from abc import abstractmethod
import graphene
from django.core.exceptions import ValidationError, PermissionDenied
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from graphql import GraphQLError, ResolveInfo


logger = logging.getLogger(__name__)

class AbstractMutation(graphene.Mutation):
    @classmethod
    def mutate(cls, *args, **kwargs):
        try:
            # if we are not in an atomic transaction, wrap with new one
            cxn = transaction.get_connection()
            if not cxn.in_atomic_block:
                with transaction.atomic():
                    return cls.do_mutate(*args, **kwargs)
            else:
                return cls.do_mutate(*args, **kwargs)
        except GraphQLError:
            raise
        except ValidationError as e:
            raise GraphQLError(_("Validation error: %(message)s") % {'message': e.message})
        except PermissionDenied as e:
            raise GraphQLError(_("Permission denied: %(message)s") % {'message': str(e)})
        except NotImplementedError:
            raise GraphQLError(_("This functionality is not yet supported"))
        except Exception as e:
            logger.exception("Unexpected error occurred")
            raise e

    @classmethod
    @abstractmethod
    def do_mutate(cls, *args, **kwargs):
        raise NotImplementedError()