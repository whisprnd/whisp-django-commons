#!/usr/bin/env python
# -*- coding: utf-8 -*-
import graphene


class MoneyNode(graphene.ObjectType):
    amount = graphene.Float(required=True)
    currency = graphene.String(required=True)

    class Meta:
        name = "Money"

