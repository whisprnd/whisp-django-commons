#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from rest_framework.test import APIClient


class GraphQLQueryExecutor:
    def __init__(self) -> None:
        super().__init__()
        self.factory = APIClient()

    def execute_query(self, query: str, op_name: str = None, variables: dict = None, jwt_token=None):
        """
        Args:
            query (string) - GraphQL query to run
            op_name (string) - If the query is a mutation or named query, you must
                               supply the op_name.  For anonymous queries ("{ ... }"),
                               should be None (default).
            variables (dict) - If provided, the $input variable in GraphQL will be set
                           to this value
            jwt_token (string) - User's JEW token
        Returns:
            dict, response from graphql endpoint.  The response has the "data" key.
                  It will have the "error" key if any error happened.
        """
        body = {'query': query}
        if op_name:
            body['operation_name'] = op_name
        if variables:
            body['variables'] = variables  # {'input': input} # relevant only for relay

        if jwt_token:
            self.factory.credentials(**{'HTTP_AUTHORIZATION': f'Bearer {jwt_token}'})
        resp = self.factory.post("/graphql", body, format='json')
        # request.user = user
        # resp = csrf_exempt(LoggingGraphQLView.as_view())(request)

        jresp = json.loads(resp.content.decode())
        return jresp
