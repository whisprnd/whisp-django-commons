from functools import wraps
from graphql_jwt.decorators import context
from graphql import GraphQLError
from django.utils.translation import ugettext_lazy as _


def login_required(f):
    @wraps(f)
    @context(f)
    def wrapper(context, *args, **kwargs):
        if not context.user.is_authenticated or not context.user.is_active:
            raise GraphQLError(_('You do not have permission to perform this action'))
        return f(*args, **kwargs)

    return wrapper