import logging
import traceback
from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from graphene_django.views import GraphQLView
from graphql import GraphQLError

logger = logging.getLogger('graphql')


class LoggingGraphQLView(GraphQLView):
    MAX_MESSAGE_LENGTH = 500

    def execute_graphql_request(self, request, data, query, variables, operation_name, show_graphiql=False):
        start = datetime.now()
        variables = variables or {}
        variables_str = ', '.join([f'{x}={v}' for x, v in variables.items()])

        if 'mutation' in data.get('query', ''):
            log_level = logging.INFO
            op_type = 'mutation'
        else:
            log_level = logging.DEBUG
            op_type = 'query'

        logger.log(log_level, f'[{op_type}] incoming {operation_name}({variables_str})')
        result = super().execute_graphql_request(request, data, query, variables, operation_name, show_graphiql)

        # handling errors
        if result.errors:
            for error in result.errors:
                try:
                    if hasattr(error, 'original_error'):
                        raise error.original_error
                    raise error
                except Exception as e:
                    # sentry_client.captureException()
                    if hasattr(e, 'stack'):
                        traceback.print_tb(e.stack)
                    logger.exception(e)

        first_result = next(iter(result.data.items()), ('', ''))[1] if result and result.data else 'None'
        response_string = str(first_result)
        response_string = (
            response_string[: self.MAX_MESSAGE_LENGTH] + '...'
            if len(response_string) > self.MAX_MESSAGE_LENGTH
            else response_string
        )

        time_taken = datetime.now() - start
        logger.log(
            log_level, f'[{op_type}] finished {operation_name} in {time_taken.total_seconds()}s ({response_string})'
        )

        return result

    @staticmethod
    def format_error(error):
        logger.info(f"Graphql error {error}. Paths: {getattr(error, 'path', 'None')}")

        if hasattr(error, 'original_error') and not isinstance(error.original_error, GraphQLError):
            return {'message': str(_('Unknown generic error'))}
        elif not isinstance(error, GraphQLError):
            return {'message': str(_('Unknown generic error'))}

        return GraphQLView.format_error(error)
