from .forms import DynamicChoicesWidgetDecorator, DynamicWidgetDecorator


class DependantAdminFieldConfigurationError(Exception):
    pass


class DependantAdminFieldsMixin:
    class Media:
        js = ('js/dynamic_model_field.js',)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not self.get_dependant_model_fields:
            return form

        for field_name, configuration in self.get_dependant_model_fields(add=(obj is None)).items():
            field = form.base_fields.get(field_name)
            if not field:
                continue
            source_field_name = configuration['source_field']
            get_new_data_url = configuration['get_new_data_url']
            type = configuration.get('type', 'choices')
            if type == 'choices':
                field.widget = DynamicChoicesWidgetDecorator().decorate(
                    field.widget, field_name, source_field_name, get_new_data_url
                )
            elif type == 'custom':
                field.widget = DynamicWidgetDecorator().decorate(
                    field.widget, field_name, source_field_name, get_new_data_url
                )
            else:
                raise DependantAdminFieldConfigurationError(f'Missing or unknown type for field {field_name}')
        return form
