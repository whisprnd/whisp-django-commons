from celery import Task
from celery.utils.log import get_task_logger
from celery_once import QueueOnce

logger = get_task_logger(__name__)


class LogErrorsTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.exception('Celery task failure', exc_info=exc)
        super(LogErrorsTask, self).on_failure(exc, task_id, args, kwargs, einfo)


class LogErrorsTaskOnce(LogErrorsTask, QueueOnce):
    pass
