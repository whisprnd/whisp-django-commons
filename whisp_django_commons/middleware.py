import json
import logging
import time


class RequestLogMiddleware(object):
    MAX_LOG_LENGTH = 200

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.start_time = time.time()
        response = self.get_response(request)

        if request.method not in ['POST', 'PATCH']:
            return response

        if 'graphql' in request.get_full_path():
            return response

        if getattr(response, 'streaming', False):
            response_type = '<<<Streaming>>> '
        else:
            response_type = ''

        log_data = {
            'status_code': response.status_code,
            'run_time': f'{time.time() - request.start_time:.2f}s',
            'params': request.POST or request.GET,
        }

        log_data_string = json.dumps(log_data)
        log_data_string = f'{log_data_string[:self.MAX_LOG_LENGTH]}...' if len(
            log_data_string) > self.MAX_LOG_LENGTH else log_data_string
        logging.info(f'[{request.get_full_path()}] {response_type}{log_data_string}')

        return response
