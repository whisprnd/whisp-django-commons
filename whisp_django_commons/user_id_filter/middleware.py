import logging

from hever.core.user_id_filter import local

try:
    from django.utils.deprecation import MiddlewareMixin
except ImportError:
    MiddlewareMixin = object

logger = logging.getLogger(__name__)


class UserIDMiddleware(MiddlewareMixin):
    def process_request(self, request):
        local.user_id = request.user and request.user.id

    def process_response(self, request, response):
        try:
            del local.user_id
        except AttributeError:
            pass

        return response
