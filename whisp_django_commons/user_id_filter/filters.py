import logging

from hever.core.user_id_filter import local, NO_USER_ID


class UserIDFilter(logging.Filter):
    def filter(self, record):
        record.user_id = getattr(local, 'user_id', NO_USER_ID)
        return True
